#!/usr/bin/env python3

from smb.SMBConnection import SMBConnection
import os, re
import time
import tarfile
import subprocess
import tempfile
import filecmp

username = 'Thorre'
password = 'ThFr@854411'
netbios_name = 'THORRENAS' # netbios name of remote server
sharename = 'multimedia' # share name of remote server
# sharename = 'multimedia' # share name of remote server
server_ip = '192.168.178.2'
client = subprocess.Popen(['hostname'],stdout=subprocess.PIPE).communicate()[0].strip()


class SmbClient(object):
        def __init__(self,ip,username,password,sharename):
                self.ip = ip
                self.username = username
                self.password = password
                self.sharename = sharename

        def connect(self):
                localhost = self.get_localhost()
                self.server = SMBConnection(username=self.username, password=self.password,
                    my_name=str(localhost), remote_name=netbios_name, use_ntlm_v2=True)
                self.server.connect(self.ip,139)


        def upload(self, path, file_name):
            file_attributes = self.file_exists(path + '/' + file_name)
            if file_attributes is None:
                data = open(file_name,'rb')
                file_name = '/' + file_name
                self.server.storeFile(self.sharename, file_name, data)
                print(f'file {file_name} has been uploaded')
            else:
                print(f'file "{file_name}" already exists')
            # data = open(file,'rb')
            # file = '/' + file
            # self.server.storeFile(self.sharename,file,data)
            # print(f'file {file} has been uploaded')

        def download_single_file(self,file, path=u'/'):
            path = path + '/' + file
            f_exists = self.file_exists(path)
            if f_exists:
                fileobj = open(file,'wb')
                file = '/' + file

                self.server.retrieveFile(self.sharename, path=file, file_obj=fileobj)
                print(f'file has been downloaded in current dir')
            else:
                print(f'file not found on servers share directory: {self.sharename}')    


        def download_all_files(self, path): 
            filelist = self.server.listPath(self.sharename, path)
            for f in filelist:
                if f.filename != '.' and f.filename != '..':
                    if f.isDirectory:
                        print(f'++++++++++++++++ {f.filename.upper()} +++++++++++++++++')
                        dir = f.filename
                        dir = path + '/' + dir
                        self.download_all_files(dir)
                    print(f.filename)



        def delete(self,file):
                f'remove file from remote share'
                file = '/' + file
                self.server.deleteFiles(self.sharename,file)

        def list(self, path):
            f' list files of remote share '
            # filelist = self.server.listPath(self.sharename,'/Musik')
            print(f'{path}')


            filelist = self.server.listPath(self.sharename, path)
            for f in filelist:
                    print(f.filename)

        def get_localhost(self):
            return subprocess.Popen(['hostname'],stdout=subprocess.PIPE).communicate()[0].strip()

        def file_exists(self, file):
            try:
                return self.server.getAttributes(service_name=self.sharename, path=file)

                # if attr is not None:
                #     return True
            except Exception as e:
                return None






file = '/home/thorsten/Repositories/python/lobhudelei.py'
local_path = '/home/thorsten/Musik/MP3s/Discofox'
remote_path = u'/Musik/MP3s/Discofox'
# remote_path = '/'

def start():
    smb = SmbClient(server_ip, username, password, sharename)
    smb.connect()

    # smb.list(path='/Musik/MP3s/Discofox')
    # smb.list(path=remote_path)

    # smb.upload(remote_path, 'synchronize.py')
    # smb.download_single_file(f'Frank Neuenfels - Weißer Engel.mp3', path=remote_path)
    # smb.download_all_files(path=remote_path)
    with os.scandir(local_path) as dirs:
        for entry in dirs:
            print(f'{entry.name}')
            # smb.upload(remote_path, entry.name)

